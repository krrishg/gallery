package np.com.krrish;

import javax.imageio.*;
import java.io.*;
import java.nio.file.*;
import java.util.*;

public class ImageInteractor {
    private final String uploadDir;

    public ImageInteractor(String uploadDir) {
        this.uploadDir = uploadDir;
    }

    private static void validate(File image) throws IOException {
        if (image == null)
            throw new DomainViolationException("Image should be provided");

        if (ImageIO.read(image) == null)
            throw new DomainViolationException("Image file is only supported");
    }

    private static Path pathOf(String location) {
        return Paths.get(location);
    }

    public void execute(File image) throws IOException {
        try {
            upload(image);
        } catch (NoSuchFileException e) {
            throw new DomainViolationException("Invalid upload directory");
        }
    }

    private void upload(File image) throws IOException {
        validate(image);
        Path source = pathOf(image.getAbsolutePath());
        Path destination = pathOf(uploadDir + "/" + image.getName());
        Files.copy(source, destination);
    }

    public List<File> listImages() throws IOException {
        File[] files = new File(uploadDir).listFiles();
        List<File> result = new ArrayList<>();

        for (File file : files) {
            if (ImageIO.read(file) != null)
                result.add(file);
        }

        return result;
    }
}
