package np.com.krrish;

public class DomainViolationException extends RuntimeException {
    public DomainViolationException(String errorMessage) {
        super(errorMessage);
    }
}
