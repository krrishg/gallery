package np.com.krrish;

import org.junit.jupiter.api.*;

import java.io.*;
import java.nio.file.*;

import static org.junit.jupiter.api.Assertions.*;

public class ImageInteractorTest {
    private String uploadPath = "testResources/uploads";

    @BeforeEach
    void setUp() throws IOException {
        Files.deleteIfExists(Paths.get(uploadPath + "/image.jpeg"));
        Files.deleteIfExists(Paths.get(uploadPath + "/test.txt"));
    }

    @Test
    void canSaveImage() throws IOException {
        File file = readFile("image.jpeg");
        imageInteractor().execute(file);
        assertEquals("image.jpeg", imageInteractor().listImages().get(0).getName());
    }

    private ImageInteractor imageInteractor() {
        return new ImageInteractor(new File(uploadPath).getAbsolutePath());
    }

    @Test
    void nonImageFilesAreNotSaved() {
        File file = readFile("test.txt");
        DomainViolationException ex = assertThrows(DomainViolationException.class,
                () -> imageInteractor().execute(file));
        assertEquals("Image file is only supported", ex.getMessage());
    }

    private File readFile(String s) {
        return new File(getClass().getClassLoader().getResource(s).getFile());
    }

    @Test
    void nullImagesAreNotSaved() {
        DomainViolationException ex = assertThrows(DomainViolationException.class,
                () -> imageInteractor().execute(null));
        assertEquals("Image should be provided", ex.getMessage());
    }

    @Test
    void nonExistingUploadPathIsNotAllowed() {
        this.uploadPath = "/nothing";
        DomainViolationException ex = assertThrows(DomainViolationException.class,
                () -> imageInteractor().execute(readFile("image.jpeg")));
        assertEquals("Invalid upload directory", ex.getMessage());
    }

    @Test
    void whenListImages_thenOnlyImagesAreListed() throws IOException {
        File file = readFile("test.txt");
        Files.copy(Paths.get(file.getAbsolutePath()), Paths.get(uploadPath + "/test.txt"));
        assertEquals(0, imageInteractor().listImages().size());
    }
}
